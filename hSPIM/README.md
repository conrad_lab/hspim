mBits_hSPIM.7z : required for installing the hSPIM software

Tutorial_Data.7z : tutorial and data demonstrating how to use the hSPIM software  

This software was developed in collaboration with Markus Fangerau and Ingmar Gergel at mBits imaging GmbH and is free for academic use.